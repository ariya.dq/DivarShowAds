package com.example.divarshowads.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity//(tableName = "ad_object")
data class AdvertisementObjectEntity(
    @PrimaryKey(autoGenerate = true) val adObjectId: Long = 0L,
    val widgetEntityList: List<WidgetEntity>,
    val lastPostDate: Long
)

@Entity//(tableName = "ad_widget")
data class WidgetEntity(
    @PrimaryKey(autoGenerate = true) val adWidgetId: Long = 0L,
    val widgetType: String,
    val data: AdvertisementEntity? = null,
    val text: String? = null,
    val adObjectId: Long = 0L,
)

@Entity//(tableName = "ad_item")
data class AdvertisementEntity(
    @PrimaryKey(autoGenerate = true) val adItemId: Long = 0L,
    val title: String?,
    val token: String?,
    val price: String?,
    val subtitle: String?,
    val thumbnail: String?,
    val city: String?,
    val district: String?,
    val adWidgetId: Long = 0L,
)
