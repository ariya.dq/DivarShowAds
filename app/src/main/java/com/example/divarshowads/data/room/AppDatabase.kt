package com.example.divarshowads.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.divarshowads.data.room.converters.AdvertisementConverter
import com.example.divarshowads.data.room.converters.AdvertisementDetailConverter
import com.example.divarshowads.data.room.converters.AdvertisementDetailWidgetConverter
import com.example.divarshowads.data.room.converters.CentroidConverter
import com.example.divarshowads.data.room.converters.CityListConverter
import com.example.divarshowads.data.room.converters.ItemListConverter
import com.example.divarshowads.data.room.converters.WidgetListConverter
import com.example.divarshowads.data.room.entity.AdvertisementEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailObjectEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity
import com.example.divarshowads.data.room.entity.AdvertisementObjectEntity
import com.example.divarshowads.data.room.entity.CentroidEntity
import com.example.divarshowads.data.room.entity.CityEntity
import com.example.divarshowads.data.room.entity.CityObjectEntity
import com.example.divarshowads.data.room.entity.ItemEntity
import com.example.divarshowads.data.room.entity.WidgetEntity

@Database(
    entities = [
        AdvertisementDetailObjectEntity::class,
        AdvertisementDetailWidgetEntity::class,
        AdvertisementDetailEntity::class,
        ItemEntity::class,
        AdvertisementObjectEntity::class,
        WidgetEntity::class,
        AdvertisementEntity::class,
        CityObjectEntity::class,
        CityEntity::class,
        CentroidEntity::class,
    ],
    version = 1,
    exportSchema = true
)

@TypeConverters(
    AdvertisementDetailWidgetConverter::class,
    AdvertisementDetailConverter::class,
    ItemListConverter::class,
    WidgetListConverter::class,
    AdvertisementConverter::class,
    CityListConverter::class,
    CentroidConverter::class,
    )
abstract class AppDatabase : RoomDatabase() {
    abstract fun advertisementListDao(): AdvertisementListDao
}