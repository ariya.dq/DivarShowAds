package com.example.divarshowads.data.repositoryImpl

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.divarshowads.data.pagingSource.AdvertisementPagingSource
import com.example.divarshowads.data.remote.service.ApiService
import com.example.divarshowads.data.remote.ApiCall.makeApiCall
import com.example.divarshowads.data.remote.model.CityObjectModel
import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.data.room.AppDatabase
import com.example.divarshowads.domain.repository.AdsRepository
import com.example.divarshowads.utils.Constant
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AdsRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
//    private val appDatabase: AppDatabase,
) : AdsRepository {

    override suspend fun getCityList(): Result<CityObjectModel> = makeApiCall {
        apiService.getCityList()
    }


    override suspend fun getAds(
        selectedCityId: Int
    ): Flow<PagingData<WidgetModelItem>> =
        Pager(
            config = PagingConfig(
                pageSize = Constant.DEFAULT_PAGE_SIZE,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                AdvertisementPagingSource(
                    api = apiService,
                    selectedCityId = selectedCityId,
//                    appDatabase = appDatabase,
                )
            }
        ).flow

    override suspend fun getAdvertisementDetail(postToken: String): Result<AdvertisementDetailObjectModel> = makeApiCall {
        apiService.getAdvertisementDetail(postToken)
    }
}
