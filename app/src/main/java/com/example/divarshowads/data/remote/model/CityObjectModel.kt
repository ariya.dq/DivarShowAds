package com.example.divarshowads.data.remote.model


data class CityObjectModel(
    val cities: List<CityModelItem>
)

data class CityModelItem(
    val centroidModelItem: CentroidModelItem,
    val id: Int,
    val name: String,
    val radius: Int,
    val slug: String
)

data class CentroidModelItem(
    val latitude: Double,
    val longitude: Double
)