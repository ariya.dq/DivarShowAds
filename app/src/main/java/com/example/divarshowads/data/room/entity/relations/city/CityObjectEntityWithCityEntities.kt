package com.example.divarshowads.data.room.entity.relations.city

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.CityEntity
import com.example.divarshowads.data.room.entity.CityObjectEntity

data class CityObjectEntityWithCityEntities(

    @Embedded val cityObjectEntity: CityObjectEntity,

    @Relation(
        parentColumn = "cityObjectId",
        entityColumn = "cityObjectId"
    )
    val cities: List<CityEntity>
)