package com.example.divarshowads.data.room.converters

import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.WidgetEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class WidgetListConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromWidgetList(widgetEntityList: List<WidgetEntity>?): String? {
        return gson.toJson(widgetEntityList)
    }

    @TypeConverter
    fun toWidgetList(widgetListString: String?): List<WidgetEntity>? {
        val listType = object : TypeToken<List<WidgetEntity>>() {}.type
        return gson.fromJson(widgetListString, listType)
    }
}