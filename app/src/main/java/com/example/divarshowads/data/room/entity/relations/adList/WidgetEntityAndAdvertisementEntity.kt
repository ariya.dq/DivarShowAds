package com.example.divarshowads.data.room.entity.relations.adList

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementEntity
import com.example.divarshowads.data.room.entity.WidgetEntity

data class WidgetEntityAndAdvertisementEntity(

    @Embedded val widgetEntity: WidgetEntity,

    @Relation(
        parentColumn = "adWidgetId",
        entityColumn = "adWidgetId"
    )
    val advertisementEntity: AdvertisementEntity

)