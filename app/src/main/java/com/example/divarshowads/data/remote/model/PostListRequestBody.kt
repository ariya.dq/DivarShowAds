package com.example.divarshowads.data.remote.model

import com.google.gson.annotations.SerializedName

data class PostListRequestBody(
    @SerializedName("last_post_date")
    val lastPostDate: Long? = 0,
    val page: Int? = 0
)