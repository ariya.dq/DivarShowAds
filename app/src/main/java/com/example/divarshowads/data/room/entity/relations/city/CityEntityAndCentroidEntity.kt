package com.example.divarshowads.data.room.entity.relations.city

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.CentroidEntity
import com.example.divarshowads.data.room.entity.CityEntity

data class CityEntityAndCentroidEntity(

    @Embedded val cityEntity: CityEntity,

    @Relation(
        parentColumn = "cityId",
        entityColumn = "cityId"
    )
    val centroidEntity: CentroidEntity

)