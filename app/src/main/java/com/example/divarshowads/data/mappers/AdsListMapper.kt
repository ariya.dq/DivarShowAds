package com.example.divarshowads.data.mappers


import com.example.divarshowads.data.remote.model.AdvertisementModelItem
import com.example.divarshowads.data.remote.model.AdvertisementObjectModel
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.data.room.entity.AdvertisementEntity
import com.example.divarshowads.data.room.entity.AdvertisementObjectEntity
import com.example.divarshowads.data.room.entity.WidgetEntity

fun AdvertisementObjectModel.toAdvertisementObjectEntity(): AdvertisementObjectEntity {
    return AdvertisementObjectEntity(
        widgetEntityList = widgetModelItemList.map { it.toWidgetEntity() },
        lastPostDate = lastPostDate
    )
}

fun WidgetModelItem.toWidgetEntity(): WidgetEntity {
    return WidgetEntity(
        widgetType = widgetType,
        data = data?.toAdvertisementEntity(),
        text = text
    )
}

fun AdvertisementModelItem.toAdvertisementEntity(): AdvertisementEntity {
    return AdvertisementEntity(
        title = title,
        token = token,
        price = price,
        subtitle = subtitle,
        thumbnail = thumbnail,
        city = city,
        district = district
    )
}

