package com.example.divarshowads.data.room.entity.relations.adList

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementObjectEntity
import com.example.divarshowads.data.room.entity.WidgetEntity


data class AdvertisementObjectEntityWithWidgetEntitiesAndAdvertisementEntities(
    @Embedded val advertisementObjectEntity: AdvertisementObjectEntity,
    @Relation(
        entity = WidgetEntity::class,
        parentColumn = "adObjectId",
        entityColumn = "adObjectId"
    )
    val widgets: WidgetEntityAndAdvertisementEntity
)