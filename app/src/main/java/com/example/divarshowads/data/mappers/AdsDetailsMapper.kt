package com.example.divarshowads.data.mappers


import com.example.divarshowads.data.remote.model.AdvertisementDetailModelItem
import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.data.remote.model.AdvertisementDetailWidget
import com.example.divarshowads.data.remote.model.Item
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailObjectEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity
import com.example.divarshowads.data.room.entity.ItemEntity

fun AdvertisementDetailObjectModel.toAdvertisementDetailObjectEntity(): AdvertisementDetailObjectEntity {
    return AdvertisementDetailObjectEntity(
        widgets = widgets?.map { it.toAdvertisementDetailWidgetEntity() },
        enableContact = enableContact,
        contactButtonText = contactButtonText
    )
}

fun AdvertisementDetailWidget.toAdvertisementDetailWidgetEntity(): AdvertisementDetailWidgetEntity {
    return AdvertisementDetailWidgetEntity(
        widgetType = widgetType,
        data = data?.toAdvertisementDetailEntity(),
    )
}

fun AdvertisementDetailModelItem.toAdvertisementDetailEntity(): AdvertisementDetailEntity {
    return AdvertisementDetailEntity(
        itemEntities = items?.map { it.toItemEntity() },
        title = title,
        subtitle = subtitle,
        value = value,
        text = text,
        imageUrl = imageUrl,
        showThumbnail = showThumbnail
    )
}


fun Item.toItemEntity(): ItemEntity {
    return ItemEntity(
        imageUrl = imageUrl
    )
}