package com.example.divarshowads.data.pagingSource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.divarshowads.data.remote.model.PostListRequestBody
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.data.remote.service.ApiService
import com.example.divarshowads.data.room.AppDatabase
import com.example.divarshowads.utils.Constant.DEFAULT_PAGE_INDEX
import com.example.divarshowads.utils.Constant.DEFAULT_PAGE_SIZE
import retrofit2.HttpException
import java.io.IOException

class AdvertisementPagingSource(
    private val api: ApiService,
    private val selectedCityId: Int,
//    private val appDatabase: AppDatabase,

) : PagingSource<Int, WidgetModelItem>() {

    companion object {
        var lastPostData: Long? = 0
    }

    override fun getRefreshKey(state: PagingState<Int, WidgetModelItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, WidgetModelItem> {
        val page = params.key ?: DEFAULT_PAGE_INDEX

        return try {
            val response = api.getAds(
                selectedCityId = selectedCityId,
                request = PostListRequestBody(
                    lastPostDate = lastPostData, page = page
                ),

                )

            lastPostData = response.lastPostDate
//            appDatabase.advertisementListDao().upsertAdsList(response.toAdvertisementObjectEntity())


            LoadResult.Page(
                data = response.widgetModelItemList,
                prevKey = if (page == DEFAULT_PAGE_SIZE) 0 else page.minus(DEFAULT_PAGE_INDEX),
                nextKey = if (response.widgetModelItemList.isEmpty()) 0 else page.plus(1)
            )

        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

}
