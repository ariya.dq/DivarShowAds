package com.example.divarshowads.data.room.entity

import androidx.room.Entity


@Entity
data class PostListRequestBodyEntity(
    val lastPostDate: Long? = 0,
    val page: Int? = 0
)