package com.example.divarshowads.data.room.entity.relations.adList

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementObjectEntity
import com.example.divarshowads.data.room.entity.WidgetEntity

data class AdvertisementObjectEntityWithWidgetEntities(

    @Embedded val advertisementObjectEntity: AdvertisementObjectEntity,

    @Relation(
        parentColumn = "adObjectId",
        entityColumn = "adObjectId"
    )
    val widgetEntities: List<WidgetEntity>
)