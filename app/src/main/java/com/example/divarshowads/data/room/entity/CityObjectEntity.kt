package com.example.divarshowads.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity//(tableName = "city_object")
data class CityObjectEntity(
    @PrimaryKey(autoGenerate = true) val cityObjectId: Long = 0L,
    val cities: List<CityEntity>
)

@Entity//(tableName = "city")
data class CityEntity(
    @PrimaryKey(autoGenerate = true) val cityId: Long = 0L,
    val centroidEntity: CentroidEntity,
    val id: Int,
    val name: String,
    val radius: Int,
    val slug: String,
    val cityObjectId: Long = 0L,
)

@Entity//(tableName = "centroid")
data class CentroidEntity(
    @PrimaryKey(autoGenerate = true) val centroidId: Long = 0L,
    val latitude: Double,
    val longitude: Double,
    val cityId: Long = 0L,
)
