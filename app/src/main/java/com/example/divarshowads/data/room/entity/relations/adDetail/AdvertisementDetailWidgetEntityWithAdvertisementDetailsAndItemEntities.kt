package com.example.divarshowads.data.room.entity.relations.adDetail

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity


data class AdvertisementDetailWidgetEntityWithAdvertisementDetailsAndItemEntities(
    @Embedded val advertisementDetailWidgetEntity: AdvertisementDetailWidgetEntity,
    @Relation(
        entity = AdvertisementDetailEntity::class,
        parentColumn = "adDetailWidgetId",
        entityColumn = "adDetailWidgetId"
    )
    val advertisementDetailsEntity: List<AdvertisementDetailEntityWithItemEntities>
)