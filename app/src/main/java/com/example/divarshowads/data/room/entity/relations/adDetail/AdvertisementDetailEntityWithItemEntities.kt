package com.example.divarshowads.data.room.entity.relations.adDetail

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.example.divarshowads.data.room.entity.ItemEntity

data class AdvertisementDetailEntityWithItemEntities(

    @Embedded val advertisementDetailEntity: AdvertisementDetailEntity,

    @Relation(
        parentColumn = "adDetailId",
        entityColumn = "adDetailId"
    )
    val itemEntities: List<ItemEntity>
)