package com.example.divarshowads.data.room.converters


import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.CentroidEntity
import com.google.gson.Gson

class CentroidConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromCentroid(centroidEntity: CentroidEntity?): String? {
        return gson.toJson(centroidEntity)
    }

    @TypeConverter
    fun toCentroid(centroidString: String?): CentroidEntity? {
        return gson.fromJson(centroidString, CentroidEntity::class.java)
    }
}
