package com.example.divarshowads.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity//(tableName = "ad_detail_object")
data class AdvertisementDetailObjectEntity(
    @PrimaryKey(autoGenerate = true) val adDetailObjectId: Long = 0L,
    val widgets: List<AdvertisementDetailWidgetEntity>?,
    val enableContact: Boolean?,
    val contactButtonText: String?
)

@Entity//(tableName = "ad_detail_widget")
data class AdvertisementDetailWidgetEntity(
    @PrimaryKey(autoGenerate = true) val adDetailWidgetId: Long = 0L,
    val widgetType: String,
    val data: AdvertisementDetailEntity?,
    val adDetailObjectId: Long = 0L
)

@Entity//(tableName = "ad_detail")
data class AdvertisementDetailEntity(
    @PrimaryKey(autoGenerate = true) val adDetailId: Long = 0L,
    val itemEntities: List<ItemEntity>? = null,
    val title: String? = null,
    val subtitle: String? = null,
    val value: String? = null,
    val text: String? = null,
    val imageUrl: String? = null,
    val showThumbnail: Boolean? = false,
    val adDetailWidgetId: Long = 0L,
)

@Entity//(tableName = "ad_detail_item")
data class ItemEntity(
    @PrimaryKey(autoGenerate = true) val itemId: Long = 0L,
    val imageUrl: String,
    val adDetailId: Long = 0L,
)
