package com.example.divarshowads.data.room.converters


import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.CityEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CityListConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromCityList(cityEntityList: List<CityEntity>?): String? {
        return gson.toJson(cityEntityList)
    }

    @TypeConverter
    fun toCityList(cityListString: String?): List<CityEntity>? {
        val listType = object : TypeToken<List<CityEntity>>() {}.type
        return gson.fromJson<List<CityEntity>>(cityListString, listType)
    }
}
