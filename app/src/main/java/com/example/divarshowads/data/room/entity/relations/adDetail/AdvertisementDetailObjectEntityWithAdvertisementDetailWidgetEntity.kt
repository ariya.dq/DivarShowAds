package com.example.divarshowads.data.room.entity.relations.adDetail

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementDetailObjectEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity

data class AdvertisementDetailObjectEntityWithAdvertisementDetailWidgetEntity(

    @Embedded val advertisementDetailObjectEntity: AdvertisementDetailObjectEntity,

    @Relation(
        parentColumn = "adDetailObjectId",
        entityColumn = "adDetailObjectId"
    )
    val advertisementDetailWidgetEntities: List<AdvertisementDetailWidgetEntity>
)