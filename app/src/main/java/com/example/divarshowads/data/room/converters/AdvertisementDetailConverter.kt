package com.example.divarshowads.data.room.converters


import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.google.gson.Gson

class AdvertisementDetailConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromAdvertisementDetail(advertisementDetailEntity: AdvertisementDetailEntity?): String? {
        return advertisementDetailEntity?.let {
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toAdvertisementDetail(advertisementDetailString: String?): AdvertisementDetailEntity? {
        return advertisementDetailString?.let {
            gson.fromJson(it, AdvertisementDetailEntity::class.java)
        }
    }
}
