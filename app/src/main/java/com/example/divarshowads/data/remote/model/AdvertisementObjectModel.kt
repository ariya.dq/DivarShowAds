package com.example.divarshowads.data.remote.model

import com.google.gson.annotations.SerializedName


data class AdvertisementObjectModel(
    @SerializedName("widget_list")
    val widgetModelItemList: List<WidgetModelItem>,
    @SerializedName("last_post_date")
    val lastPostDate: Long
)

data class WidgetModelItem(
    @SerializedName("widget_type")
    val widgetType: String,
    val data: AdvertisementModelItem? = null,
    val text: String? = null
)

data class AdvertisementModelItem(
    val title: String?,
    val token: String?,
    val price: String?,
    val subtitle: String,
    val thumbnail: String?,
    val city: String,
    val district: String?,

)