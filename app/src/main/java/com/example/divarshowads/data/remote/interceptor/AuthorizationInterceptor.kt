package com.example.divarshowads.data.remote.interceptor

import com.example.divarshowads.data.local.UserPreferencesRepository
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthorizationInterceptor @Inject constructor(
    private val userPreferencesRepository: UserPreferencesRepository
) : Interceptor{

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val builder = request.newBuilder()
        builder.addHeader(
            "Content-Type",
            "application/json"
        )
        if(userPreferencesRepository.getToken() != "")
        {
            builder.addHeader(
                "x-access-token",
                String.format("Basic %s", userPreferencesRepository.getToken())
            )
        }
        else{
            builder.removeHeader("Authorization")
        }
        request = builder.build()

        return chain.proceed(request)

    }

}