package com.example.divarshowads.data.room

import androidx.room.Dao
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.divarshowads.data.room.entity.AdvertisementObjectEntity
import com.example.divarshowads.data.room.entity.relations.adList.AdvertisementObjectEntityWithWidgetEntitiesAndAdvertisementEntities

@Dao
interface AdvertisementListDao {

    @Query("SELECT * FROM AdvertisementObjectEntity")
    suspend fun getAdList(): List<AdvertisementObjectEntityWithWidgetEntitiesAndAdvertisementEntities>
//    suspend fun getAdList(): AdvertisementObject

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertAdsList(advertisementObjectEntity: AdvertisementObjectEntity)

    @Query("SELECT lastPostDate FROM AdvertisementObjectEntity ")
    suspend fun getLastUpdated(): Long?

}