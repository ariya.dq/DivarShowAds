package com.example.divarshowads.data.room.converters


import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity

object AdvertisementDetailWidgetConverter {
    @TypeConverter
    @JvmStatic
    fun fromString(value: String): List<AdvertisementDetailWidgetEntity> {
        val listType = object : TypeToken<List<AdvertisementDetailWidgetEntity>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromList(list: List<AdvertisementDetailWidgetEntity>): String {
        return Gson().toJson(list)
    }
}
