package com.example.divarshowads.data.remote.model

import com.google.gson.annotations.SerializedName


data class AdvertisementDetailObjectModel(
    val widgets: List<AdvertisementDetailWidget>?,
    @SerializedName("enable_contact")
    val enableContact: Boolean?,
    @SerializedName("contact_button_text")
    val contactButtonText: String?
)

data class AdvertisementDetailWidget(
    @SerializedName("widget_type")
    val widgetType: String,
    val data: AdvertisementDetailModelItem?
)

data class AdvertisementDetailModelItem(
    val items: List<Item>? = null,
    val title: String? = null,
    val subtitle: String? = null,
    val value: String? = null,
    val text: String? = null,
    @SerializedName("image_url")
    val imageUrl: String? = null,
    @SerializedName("show_thumbnail")
    val showThumbnail: Boolean? = false
)

data class Item(
    @SerializedName("image_url")
    val imageUrl: String
)