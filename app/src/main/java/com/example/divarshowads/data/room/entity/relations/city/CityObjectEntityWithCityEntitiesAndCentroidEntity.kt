package com.example.divarshowads.data.room.entity.relations.city

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.CityObjectEntity
import com.example.divarshowads.data.room.entity.WidgetEntity


data class CityObjectEntityWithCityEntitiesAndCentroidEntity(
    @Embedded val cityObjectEntity: CityObjectEntity,
    @Relation(
        entity = WidgetEntity::class,
        parentColumn = "cityObjectId",
        entityColumn = "cityObjectId"
    )
    val cities: CityEntityAndCentroidEntity
)