package com.example.divarshowads.data.remote

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ApiCall {

    suspend fun <T> makeApiCall(
        call: suspend () -> T
    ): Result<T> = runCatching {
        withContext(Dispatchers.IO) {
            call.invoke()
        }
    }

}