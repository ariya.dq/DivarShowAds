package com.example.divarshowads.data.room.entity.relations.adDetail

import androidx.room.Embedded
import androidx.room.Relation
import com.example.divarshowads.data.room.entity.AdvertisementDetailEntity
import com.example.divarshowads.data.room.entity.AdvertisementDetailWidgetEntity

data class AdvertisementDetailWidgetEntityAndAdvertisementDetailEntities(

    @Embedded val advertisementDetailWidgetEntity: AdvertisementDetailWidgetEntity,

    @Relation(
        parentColumn = "adDetailWidgetId",
        entityColumn = "adDetailWidgetId"
    )
    val advertisementDetailEntity: AdvertisementDetailEntity

)