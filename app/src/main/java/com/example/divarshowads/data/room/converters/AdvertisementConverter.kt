package com.example.divarshowads.data.room.converters


import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.AdvertisementEntity
import com.google.gson.Gson


class AdvertisementConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromAdvertisement(advertisementEntity: AdvertisementEntity?): String? {
        return gson.toJson(advertisementEntity)
    }

    @TypeConverter
    fun toAdvertisement(advertisementString: String?): AdvertisementEntity? {
        return gson.fromJson(advertisementString, AdvertisementEntity::class.java)
    }
}
