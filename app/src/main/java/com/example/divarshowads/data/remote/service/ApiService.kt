package com.example.divarshowads.data.remote.service

import com.example.divarshowads.data.remote.model.AdvertisementObjectModel
import com.example.divarshowads.data.remote.model.CityObjectModel
import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.data.remote.model.PostListRequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("place/list")
    suspend fun getCityList(): CityObjectModel

    @POST("post/list")
    suspend fun getAds(@Query("city") selectedCityId: Int, @Body request: PostListRequestBody): AdvertisementObjectModel

    @GET("post/view/{postToken}")
    suspend fun getAdvertisementDetail(@Path("postToken") postToken: String): AdvertisementDetailObjectModel


}