package com.example.divarshowads.data.room.converters

import androidx.room.TypeConverter
import com.example.divarshowads.data.room.entity.ItemEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ItemListConverter {
    private val gson = Gson()

    @TypeConverter
    fun fromItemList(itemEntityList: List<ItemEntity>?): String? {
        return gson.toJson(itemEntityList)
    }

    @TypeConverter
    fun toItemList(itemListString: String?): List<ItemEntity>? {
        val itemEntityType = object : TypeToken<List<ItemEntity>>() {}.type
        return gson.fromJson(itemListString, itemEntityType)
    }
}