package com.example.divarshowads.domain.repository

import androidx.paging.PagingData
import com.example.divarshowads.data.remote.model.CityObjectModel
import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.data.remote.model.WidgetModelItem
import kotlinx.coroutines.flow.Flow

interface AdsRepository {

    suspend fun getCityList(): Result<CityObjectModel>

    suspend fun getAds(selectedCityId: Int): Flow<PagingData<WidgetModelItem>>

    suspend fun getAdvertisementDetail(postToken: String): Result<AdvertisementDetailObjectModel>
//    suspend fun getAdvertisementObjects(): List<AdvertisementObjectWithWidgetsAndAdvertisements>

}