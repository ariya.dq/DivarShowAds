package com.example.divarshowads.domain.usecase

import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.domain.repository.AdsRepository
import javax.inject.Inject

class GetAdvertisementDetailUseCase @Inject constructor(
    private val repository: AdsRepository
) {
    suspend operator fun invoke(postToken: String): Result<AdvertisementDetailObjectModel> {
        return repository.getAdvertisementDetail(postToken)
    }
}