package com.example.divarshowads.domain.usecase

import com.example.divarshowads.data.remote.model.CityObjectModel
import com.example.divarshowads.domain.repository.AdsRepository
import javax.inject.Inject

class GetCityListUseCase @Inject constructor(
    private val repository: AdsRepository
) {
    suspend operator fun invoke(): Result<CityObjectModel> {
        return repository.getCityList()
    }
}