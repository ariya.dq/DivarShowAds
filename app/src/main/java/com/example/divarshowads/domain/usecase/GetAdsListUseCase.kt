package com.example.divarshowads.domain.usecase

import androidx.paging.PagingData
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.domain.repository.AdsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAdsListUseCase @Inject constructor(
    private val repository: AdsRepository
) {
    suspend operator fun invoke(selectedCityId: Int): Flow<PagingData<WidgetModelItem>> {
        return repository.getAds(selectedCityId = selectedCityId)
    }
}