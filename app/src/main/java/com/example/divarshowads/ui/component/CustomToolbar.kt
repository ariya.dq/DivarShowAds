package com.example.divarshowads.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.TextUnit
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens._12
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.font_24
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.ui.theme.white


@Composable
fun CustomToolbar(
    title: String,
    onClick: () -> Unit,
    titleFontSize: TextUnit = font_20,
    titleFontWeight: FontWeight = FontWeight.SemiBold,
    titleStyle: TextStyle = MaterialTheme.typography.labelLarge,
    titleLineHeight: TextUnit = font_24,
    textColor: Color = onSurface,
    backgroundColor: Color? = white,
    startIcon: Int? = null,
    arrowIcon: Int? = R.drawable.ic_back,
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(backgroundColor!!),
            verticalAlignment = Alignment.CenterVertically,
        ) {

            if (startIcon != null) {
                Image(
                    painter = painterResource(id = startIcon),
                    contentDescription = "",
                )
                Spacer(modifier = Modifier.width(_12))
            }
            Text(
                text = title,
                fontSize = titleFontSize,
                fontWeight = titleFontWeight,
                fontFamily = iranyekanFamily,
                style = titleStyle,
                lineHeight = titleLineHeight,
                color = textColor,
            )

            Spacer(modifier = Modifier.weight(1f))

            Icon(
                imageVector = ImageVector.vectorResource(id = arrowIcon!!),
                contentDescription = "",
                modifier = Modifier
                    .clickable {
                        onClick.invoke()
                    },
                tint = Color.Unspecified
            )

        }
    }
}

@Preview(showBackground = true)
@Composable
fun CustomToolbarPreview() {
    CustomToolbar(title = stringResource(R.string.test_message), onClick = {}, startIcon = null)
}
