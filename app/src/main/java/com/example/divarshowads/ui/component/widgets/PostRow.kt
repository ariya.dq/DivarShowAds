package com.example.divarshowads.ui.component.widgets

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.data.remote.model.AdvertisementModelItem
import com.example.divarshowads.ui.component.image.AdsImage
import com.example.divarshowads.ui.component.texts.FilledText
import com.example.divarshowads.ui.theme.Dimens._12
import com.example.divarshowads.ui.theme.Dimens._20
import com.example.divarshowads.ui.theme.Dimens._5
import com.example.divarshowads.ui.theme.black
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.green2
import com.example.divarshowads.ui.theme.green2_10
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.primary
import com.example.divarshowads.ui.theme.primary_10
import com.example.divarshowads.ui.theme.surface1

@Composable
fun PostRow(
    itemModel: WidgetModelItem,
    onClick: (WidgetModelItem) -> Unit
) {

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Card(
            shape = RoundedCornerShape(_12),
            colors = CardDefaults.cardColors(containerColor = surface1),
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(bottom = _20)
                .clip(RoundedCornerShape(_12))
                .clickable { onClick.invoke(itemModel) }
        ) {
            Column(modifier = Modifier.padding(_12)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Start
                ) {

                    AdsImage(imageUrl = itemModel.data?.thumbnail)

                    Spacer(modifier = Modifier.width(_12))

                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(_20)
                    ) {

                        itemModel.data?.title?.let {
                            Text(
                                text = it,
                                color = black,
                                fontFamily = iranyekanFamily,
                                fontSize = font_14,
                                fontWeight = FontWeight.Normal,
                                lineHeight = font_20,
                            )
                        }

                        Spacer(modifier = Modifier.height(_12))

                        if (!itemModel.data?.subtitle.isNullOrEmpty())
                            itemModel.data?.subtitle?.let {
                                Text(
                                    text = it,
                                    color = black,
                                    fontFamily = iranyekanFamily,
                                    fontSize = font_14,
                                    fontWeight = FontWeight.Normal,
                                    lineHeight = font_20,
                                )
                            }


                        Spacer(modifier = Modifier.height(_12))

                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Start
                        ) {

                            if (!itemModel.data?.city.isNullOrEmpty())
                                itemModel.data?.city?.let {
                                    FilledText(
                                        title = it,
                                        titleColor = green2,
                                        backgroundColor = green2_10,
                                    )
                                }

                            Spacer(modifier = Modifier.width(_5))

                            if (!itemModel.data?.district.isNullOrEmpty())
                                itemModel.data?.district?.let {
                                    FilledText(
                                        title = it,
                                        titleColor = green2,
                                        backgroundColor = green2_10,
                                    )

                                }

                        }
                    }
                }

                Spacer(modifier = Modifier.height(_12))

                itemModel.data?.price?.let {
                    FilledText(
                        title = if (itemModel.data.price.isNotEmpty()) it else stringResource(R.string.without_price),
                        titleColor = primary,
                        backgroundColor = primary_10,
                        modifier = Modifier.wrapContentWidth()
                    )
                }

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PostRowPreview() {
    PostRow(
        itemModel = WidgetModelItem(
            widgetType = stringResource(R.string.post_row),
            data = AdvertisementModelItem(
                title = stringResource(R.string.post_row_sample_data_title),
                token = stringResource(R.string.post_row_sample_data_token),
                price = stringResource(R.string.post_row_sample_data_price),
                subtitle = stringResource(R.string.post_row_sample_data_sub_title),
                thumbnail = stringResource(R.string.post_row_sample_data_thumbnail),
                city = stringResource(R.string.post_row_sample_data_city),
                district = stringResource(R.string.post_row_sample_data_district)
            )
        ),
        onClick = {}
    )
}
