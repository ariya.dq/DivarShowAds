package com.example.divarshowads.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.ui.component.texts.InputTextFieldColors
import com.example.divarshowads.ui.theme.Dimens._1
import com.example.divarshowads.ui.theme.Dimens._12
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.ui.theme.onSurfaceVariant
import com.example.divarshowads.ui.theme.outlineVariant

@Composable
fun CustomSearchLayout(
    hint: String,
    shape: Shape = RoundedCornerShape(_12),
    textInputChange: (String) -> Unit,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions(
        keyboardType = KeyboardType.Number, capitalization = KeyboardCapitalization.None
    ),
    colors: TextFieldColors = InputTextFieldColors(),
    leadingIcon: Int? = R.drawable.ic_outline_search,
    modifier: Modifier? = Modifier.fillMaxWidth(),
    onDeleteInputClick: () -> Unit,
) {
    var textInput by rememberSaveable { mutableStateOf("") }


    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        val focusManager = LocalFocusManager.current

        TextField(
            value = textInput,
            textStyle = TextStyle(
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Start,
                fontFamily = iranyekanFamily,
                color = onSurface,
                fontSize = font_14,
                lineHeight = font_16
            ),
            onValueChange = {
                textInput = it
                textInputChange.invoke(textInput)
            },
            placeholder = {
                Text(
                    text = hint,
                    fontWeight = FontWeight.Normal,
                    textAlign = TextAlign.Start,
                    fontFamily = iranyekanFamily,
                    color = onSurfaceVariant,
                    fontSize = font_12,
                    lineHeight = font_16,
                )
            },
            modifier = modifier!!.border(
                _1, outlineVariant, shape
            ),
            colors = colors,
            singleLine = true,
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
            visualTransformation = visualTransformation,
            keyboardOptions = keyboardOptions,
            shape = shape,
            leadingIcon = {
                Image(
                    painter = painterResource(id = leadingIcon!!),
                    contentDescription = null,
                )
            },
            trailingIcon = {
                if (textInput.isNotEmpty()) {
                    IconButton(
                        onClick = {
                            textInput = ""
                            onDeleteInputClick.invoke()
                        }) {
                        Icon(
                            imageVector = Icons.Outlined.Close,
                            contentDescription = null,
                            modifier = Modifier.fillMaxHeight()
                        )
                    }
                }
            }
        )
    }
}


@Preview
@Composable
fun CustomSearchLayoutPreview() {
    CustomSearchLayout(stringResource(R.string.test_message),
        textInputChange = {},
    onDeleteInputClick = {})
}