package com.example.divarshowads.ui.home.composable.adList

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._15
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.font_24
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens._12
import com.example.divarshowads.ui.theme.Dimens._8
import com.example.divarshowads.ui.theme.surfaceVariant

@Composable
fun SelectCityLayout(value: String?, onClick: () -> Unit) {

    Card(
        shape = RoundedCornerShape(_12), colors = CardDefaults.cardColors(
            containerColor = surfaceVariant,
        ),
        modifier = Modifier
            .padding(bottom = _15)
            .fillMaxWidth()
            .clickable {onClick.invoke()}


        ) {
        Row(
            modifier = Modifier.padding(horizontal = _8),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter =  painterResource(id = R.drawable.ic_location),
                contentDescription = null,
            )
            Column(
                modifier = Modifier.padding(
                    start = Dimens._16, end = Dimens._16, top = Dimens._13, bottom = Dimens._13
                ),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    text = value ?: stringResource(com.example.divarshowads.R.string.select_your_city),
                    fontSize = font_16,
                    fontWeight = FontWeight.Normal,
                    fontFamily = iranyekanFamily,
                    lineHeight = font_24,
                    color = onSurface,
                )

            }
            Spacer(modifier = Modifier.weight(1f))
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically),
                contentAlignment = Alignment.CenterEnd
            ) {
                Image(
                    painter =  painterResource(id = R.drawable.ic_fill_adornment),
                    contentDescription = null,
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SelectCityLayoutPreview() {
    SelectCityLayout(
        value = stringResource(id = R.string.test_message),
        onClick = {}
    )
}
