package com.example.divarshowads.ui.home.composable.adList.bottomSheet

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.CentroidModelItem
import com.example.divarshowads.data.remote.model.CityModelItem
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.font_24
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.ui.theme.white

@Composable
fun CityListItem(
    modelItem: CityModelItem,
    onClick: (CityModelItem) -> Unit
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Card(
            modifier = Modifier
                .padding(bottom = Dimens._15)
                .fillMaxWidth()
                .clickable {
                    onClick.invoke(modelItem)
                },

            ) {
            Row(
                modifier = Modifier.background(white),
                verticalAlignment = Alignment.CenterVertically
            ) {

                Column(
                    modifier = Modifier.padding(
                        start = Dimens._16, end = Dimens._16, top = Dimens._13, bottom = Dimens._13
                    ),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        text = modelItem.name,
                        fontSize = font_16,
                        fontWeight = FontWeight.Normal,
                        fontFamily = iranyekanFamily,
                        lineHeight = font_24,
                        color = onSurface,
                    )

                }
                Spacer(modifier = Modifier.weight(1f))
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterVertically),
                    contentAlignment = Alignment.CenterEnd
                ) {
                    Image(
                        modifier = Modifier.padding(start = Dimens._5, end = Dimens._28),
                        painter = painterResource(id = R.drawable.ic_left_arrow),
                        contentDescription = "",
                    )
                }
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun CityListItemPreview() {
    CityListItem(
        modelItem = CityModelItem(
            name = stringResource(R.string.city_name),
            id = 1,
            slug = stringResource(R.string.city_slug),
            radius = 25000,
            centroidModelItem = CentroidModelItem(
                latitude = 35.717358,
                longitude = 51.375076
            )
        ),
        onClick = {}
    )
}