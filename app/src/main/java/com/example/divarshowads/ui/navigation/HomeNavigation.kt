package com.example.divarshowads.ui.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.divarshowads.ui.home.HomeViewModel
import com.example.divarshowads.ui.navigation.Navigation.Args.AD_TITLE
import com.example.divarshowads.ui.navigation.Navigation.Args.POST_TOKEN
import com.example.divarshowads.ui.navigation.Navigation.Routes.Ad_DETAIL_SCREEN

@Composable
fun HomeNavigation() {
    val viewModel: HomeViewModel = hiltViewModel()
    val navController = rememberNavController()

    NavHost(
        navController = navController, startDestination = Navigation.Routes.AD_LIST_SCREEN
    ) {

        composable(
            route = Navigation.Routes.AD_LIST_SCREEN
        ) {
            AdListScreenDestination(navController, viewModel)
        }

        composable(
            route = Ad_DETAIL_SCREEN,
            arguments = listOf(
                navArgument(name = POST_TOKEN) {
                    type = NavType.StringType
                },
                navArgument(name = AD_TITLE) {
                    type = NavType.StringType
                }

            )
        ) { backStackEntry ->
            val postToken =
                requireNotNull(backStackEntry.arguments?.getString(POST_TOKEN)) { "postToken is required as an argument" }
            val adTitle =
                requireNotNull(backStackEntry.arguments?.getString(AD_TITLE)) { "adTitle is required as an argument" }
            AdDetailScreenDestination(
                navController = navController,
                viewModel = viewModel,
                postToken = postToken,
                adTitle = adTitle
            )
        }

    }
}

object Navigation {

    object Args {
        const val POST_TOKEN = "postToken"
        const val AD_TITLE = "adTitle"
    }
    object Routes {
        const val AD_LIST_SCREEN = "AdListScreen"
        const val Ad_DETAIL_SCREEN = "AdDetailScreen/{${POST_TOKEN}}/{${AD_TITLE}}"
    }

}
