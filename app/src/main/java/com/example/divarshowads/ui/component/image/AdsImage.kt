package com.example.divarshowads.ui.component.image

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._118


@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun AdsImage(imageUrl: String?, modifier: Modifier? = Modifier ) {

    if (imageUrl.isNullOrEmpty())
        Image(
            painter = painterResource(id = R.drawable.ic_gallery_gray),
            contentDescription = stringResource(R.string.image_gallery),
            alignment = Alignment.CenterStart,
            contentScale = ContentScale.FillBounds,
            modifier = modifier!!
                .clip(RoundedCornerShape(Dimens._12))
                .size(_118),
        )
    else
        GlideImage(
            model = imageUrl,
            contentDescription = "",
            alignment = Alignment.CenterStart,
            contentScale = ContentScale.FillBounds,
            modifier = modifier!!
                .clip(RoundedCornerShape(Dimens._12))
                .size(_118),
        )

}


@Preview(showBackground = true)
@Composable
fun AdsImagePreview() {
    AdsImage(modifier = Modifier, imageUrl = null)
}