package com.example.divarshowads.ui.home

import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.divarshowads.domain.usecase.GetAdsListUseCase
import com.example.divarshowads.domain.usecase.GetAdvertisementDetailUseCase
import com.example.divarshowads.domain.usecase.GetCityListUseCase
import com.example.divarshowads.ui.base.BaseViewModel
import com.example.divarshowads.utils.removeSlash
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getAdsListUseCase: GetAdsListUseCase,
    private val getCityListUseCase: GetCityListUseCase,
    private val getAdvertisementDetailUseCase: GetAdvertisementDetailUseCase,
) :
    BaseViewModel<HomeContract.Event, HomeContract.State, HomeContract.Effect>() {


    override fun setInitialState() = HomeContract.State(
        isLoading = true,
        isError = false,
        throwable = null,
        cityList = null,
        currentCitySelected = null,
        advertisementList = null,
        selectedAdvertisement = null,
        advertisementDetail = null,
    )

    override fun handleEvents(event: HomeContract.Event) {

        when (event) {

            is HomeContract.Event.BackButtonClicked -> {
                setState { copy(advertisementDetail = null) }
                setEffect { HomeContract.Effect.Navigation.Back }
            }

            is HomeContract.Event.OnAdvertisementClick -> {
                setState { copy(selectedAdvertisement = event.selectedAdvertisement) }
                setEffect {
                    HomeContract.Effect.Navigation.ToDetailScreen(
                        postToken = event.selectedAdvertisement.data?.token,
                        adTitle = event.selectedAdvertisement.data?.title?.removeSlash()
                    )
                }
            }
            is HomeContract.Event.OnCityListItemClick -> {
                getAdvertisementList(selectedCityId = event.selectedCityModelItem.id)
                setState { copy(currentCitySelected = event.selectedCityModelItem) }
                setEffect { HomeContract.Effect.Visibility.BottomSheetVisibility }

            }
            is HomeContract.Event.GetAdvertisementDetailApiCall -> {
                getAdvertisementDetail(postToken = event.postToken)
            }
            is HomeContract.Event.InputTextChangeIntCitySearchLayout -> {
                if ( !event.userInput.isNullOrEmpty()) {
                    var newCityList = viewState.value.cityList?.filter { item ->
                        item.name.contains(event.userInput, ignoreCase = true)
                    }
                    setState {copy(cityListResultSearch = newCityList)}
                } else {
                    setState {
                        copy(
                            cityListResultSearch= null,
                            cityList= viewState.value.cityList,
                        )
                    }
                }
            }
            HomeContract.Event.OnSelectCityLayoutClick -> {
                setEffect { HomeContract.Effect.Visibility.BottomSheetVisibility }
            }
            HomeContract.Event.RetryApiCall -> {
                setState { copy(needToRetryApiCall = false) }
                getCityList()
            }
            HomeContract.Event.InternetUnAvailable -> {
                setState { copy(needToRetryApiCall = true, isLoading = false) }
            }
            HomeContract.Event.OnClearUserInputClick -> {
                setState {
                    copy(
                        cityListResultSearch= null,
                        cityList= viewState.value.cityList,
                    )
                }
                setEffect { HomeContract.Effect.ClearUserInput }
            }
        }
    }

    private fun getCityList() = viewModelScope.launch {
        setState { copy(isError = false, isLoading = true) }

        getCityListUseCase.invoke().onSuccess { value ->
            getAdvertisementList(selectedCityId = value.cities.first().id)
            setState { copy(cityList = value.cities, currentCitySelected = value.cities.first(), isLoading = false, isError = false) }
        }.onFailure { errorMessage ->
            setState { copy(isError = true, isLoading = false, throwable = errorMessage) }

        }
    }

    private fun getAdvertisementList(selectedCityId: Int) {
        setState { copy(isError = false, isLoading = true) }
        viewModelScope.launch {
            try {
                val advertisementList = getAdsListUseCase.invoke(selectedCityId = selectedCityId).cachedIn(viewModelScope)
                setState { copy(advertisementList = advertisementList, isLoading = false) }
            }catch (e:Exception){
                setState { copy(isError = true, isLoading = false, throwable = Throwable(e)) }
            }
        }
    }

    private fun getAdvertisementDetail(postToken: String) = viewModelScope.launch {
        setState { copy(isError = false, isLoading = true) }

        getAdvertisementDetailUseCase.invoke(postToken).onSuccess { value ->
            setState { copy(advertisementDetail = value, isLoading = false, isError = false) }
        }.onFailure { errorMessage ->
            setState { copy(isError = true, isLoading = false, throwable = errorMessage) }

        }
    }


}