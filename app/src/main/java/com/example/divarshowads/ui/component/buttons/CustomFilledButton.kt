package com.example.divarshowads.ui.component.buttons


import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.TextUnit
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.primary
import com.example.divarshowads.ui.theme.surface1


@Composable
fun CustomFilledButtons(
    btnText: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    textColor: Color = primary,
    fontSize: TextUnit = font_14,
    fontWeight: FontWeight = FontWeight.Medium,
    fontFamily: FontFamily = iranyekanFamily,
    style: TextStyle = MaterialTheme.typography.labelLarge,
    lineHeight: TextUnit = font_20,
    backgroundBtnColor: Color = surface1,
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        FilledTonalButton(
            modifier = modifier,
            onClick = { onClick.invoke() },
            colors = ButtonDefaults.buttonColors(backgroundBtnColor)
        ) {
            Text(
                text = btnText,
                color = textColor,
                fontSize = fontSize,
                fontWeight = fontWeight,
                fontFamily = fontFamily,
                style = style,
                lineHeight = lineHeight,
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CustomFilledButtonsPreview() {
    CustomFilledButtons(
        btnText = stringResource(R.string.test_message),
        onClick = {},
    )
}
