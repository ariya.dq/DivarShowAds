package com.example.divarshowads.ui.home.composable.adDetail

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.AdvertisementDetailModelItem
import com.example.divarshowads.data.remote.model.AdvertisementDetailWidget
import com.example.divarshowads.data.remote.model.Item
import com.example.divarshowads.ui.component.widgets.DescriptionRow
import com.example.divarshowads.ui.component.widgets.HeaderRow
import com.example.divarshowads.ui.component.widgets.ImageSliderRow
import com.example.divarshowads.ui.component.widgets.InfoRow
import com.example.divarshowads.ui.component.widgets.SubTitleRow
import com.example.divarshowads.ui.component.widgets.TitleRow
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._60
import com.example.divarshowads.utils.ViewType

@Composable
fun GenerateListViewItems(
    list: List<AdvertisementDetailWidget>,
) {

    LazyColumn(modifier = Modifier.padding(bottom = _60)) {
        items(list) { item ->
            when(item.widgetType){

                ViewType.TITLE_ROW.name ->{
                    item.data?.text?.let { TitleRow(title = it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

                ViewType.SUBTITLE_ROW.name ->{
                    item.data?.text?.let { SubTitleRow(title = it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

                ViewType.IMAGE_SLIDER_ROW.name ->{
                    item.data?.items?.let { ImageSliderRow(it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

                ViewType.HEADER_ROW.name ->{
                    item.data?.let { HeaderRow(it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

                ViewType.INFO_ROW.name ->{
                    item.data?.let { InfoRow(it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

                ViewType.DESCRIPTION_ROW.name ->{
                    item.data?.let { DescriptionRow(it) }
                    Spacer(modifier = Modifier.height(Dimens._12))
                }

            }
        }
    }

}


@Preview(showBackground = true)
@Composable
fun GenerateListViewItems_IMAGE_SLIDER_ROW_Preview() {
    GenerateListViewItems(
        list = listOf(
            AdvertisementDetailWidget(
                widgetType = stringResource(R.string.post_row_sample_widget_type),
                data = AdvertisementDetailModelItem(
                    items = listOf(
                        Item(
                            imageUrl = stringResource(R.string.post_row_sample_data_items_image_url_1)
                        ),
                        Item(
                            imageUrl = stringResource(R.string.post_row_sample_data_items_image_url_2)
                        ),
                        Item(
                            imageUrl = stringResource(R.string.post_row_sample_data_items_image_url_3)
                        ),
                    ),
                    title = null,
                    subtitle = null,
                    value = null,
                    text = null,
                    imageUrl = null,
                    showThumbnail = null,
                )
            ),
        ),
    )
}