package com.example.divarshowads.ui.component.texts

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.TextUnit
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens._4
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.ui.theme.onSurfaceVariant

@Composable
fun CustomTextField(
    title: String,
    fontSize: TextUnit = font_16,
    fontWeight: FontWeight = FontWeight.SemiBold,
    textStyle: TextStyle = MaterialTheme.typography.labelMedium,
    fontFamily: FontFamily? = iranyekanFamily,
    lineHeight: TextUnit = font_20,
    textColor: Color = onSurface,
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Text(
            text = title,
            modifier = Modifier
                .padding(bottom = _4),
            textAlign = TextAlign.Start,
            color = textColor,
            fontSize = fontSize,
            fontFamily = fontFamily,
            fontWeight = fontWeight,
            lineHeight = lineHeight,
            style = textStyle,

            )
    }
}

@Preview(showBackground = true)
@Composable
fun CustomTextField_TitlePreview() {
    CustomTextField(
        title = stringResource(id = R.string.app_name),
    )
}

@Preview(showBackground = true)
@Composable
fun CustomTextField_SubTitlePreview() {
    CustomTextField(
        title = stringResource(id = R.string.app_name),
        fontSize = font_12,
        fontWeight = FontWeight.Normal,
        textStyle = MaterialTheme.typography.labelSmall,
        lineHeight = font_16,
        textColor = onSurfaceVariant,
    )
}