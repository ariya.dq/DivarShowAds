package com.example.divarshowads.ui.home.composable.adList.bottomSheet

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.ShapeDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.CentroidModelItem
import com.example.divarshowads.data.remote.model.CityModelItem
import com.example.divarshowads.ui.component.CustomSearchLayout
import com.example.divarshowads.ui.component.NoDataScreen
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._20
import com.example.divarshowads.ui.theme.Dimens._400
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.gray2
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.light_gray
import com.example.divarshowads.ui.theme.surface1

@Composable
fun CityBottomSheetContent(
    cityList: List<CityModelItem>?,
    cityListResultSearch: List<CityModelItem>? = null,
    onCityItemClick: (CityModelItem) -> Unit,
    onCitySearchLayoutTextChange: (String) -> Unit,
    onDeleteInputClick: () -> Unit,
    ) {
    var citySearchQuery: String by remember { mutableStateOf("") }

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
        Box(
            Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(surface1)
                .padding(start = Dimens._24, end = Dimens._24)

        ) {
            Column {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.SpaceBetween,
                    horizontalAlignment = Alignment.Start
                ) {
                    Row(
                        modifier = Modifier
                            .padding(top = Dimens._16)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Spacer(
                            Modifier
                                .width(Dimens._32)
                                .height(Dimens._4)
                                .background(gray2, shape = ShapeDefaults.Medium)
                        )
                    }
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = Dimens._16/*, start = Dimens._20*/),
                        text = stringResource(R.string.select_your_city),
                        fontSize = font_14,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = iranyekanFamily,
                        style = typography.labelLarge,
                        lineHeight = font_20,
                        color = light_gray,
                    )
                    Spacer(
                        Modifier.height(Dimens._10)
                    )

                    CustomSearchLayout(
                        hint = stringResource(R.string.search_your_city),
                        textInputChange = {
                            citySearchQuery = it
                            onCitySearchLayoutTextChange.invoke(it)
                        },
                        onDeleteInputClick = {
                            citySearchQuery = ""
                            onDeleteInputClick.invoke()
                        },
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Text, capitalization = KeyboardCapitalization.None
                        ),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = _20)
                    )

                    Spacer(
                        Modifier.height(Dimens._10)
                    )
                }


                Column(
                    Modifier
                        .fillMaxSize(),
                ) {
                    if (cityList.isNullOrEmpty() || citySearchQuery.isNotEmpty() && cityListResultSearch.isNullOrEmpty())
                        Surface(modifier = Modifier.fillMaxWidth().height(_400)) {
                            NoDataScreen()
                        }
                    else
                        CityList(list = cityList, resultSearchList = cityListResultSearch, onClick = { onCityItemClick.invoke(it) })

                }

            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun CityBottomSheetContentPreview() {
    CityBottomSheetContent(
        onCityItemClick = {},
        onCitySearchLayoutTextChange = {},
        onDeleteInputClick = {},
        cityList = listOf(
            CityModelItem(
                name = stringResource(R.string.city_name),
                id = 1,
                slug = stringResource(R.string.city_slug),
                radius = 25000,
                centroidModelItem = CentroidModelItem(
                    latitude = 35.717358,
                    longitude = 51.375076
                )
            ),
            CityModelItem(
                name = stringResource(R.string.city_name_2),
                id = 1,
                slug = stringResource(R.string.city_slug_2),
                radius = 25000,
                centroidModelItem = CentroidModelItem(
                    latitude = 35.717358,
                    longitude = 51.375076
                )
            )
        )
    )
}

