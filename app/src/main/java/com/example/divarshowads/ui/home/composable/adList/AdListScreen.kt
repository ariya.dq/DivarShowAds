package com.example.divarshowads.ui.home.composable.adList

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.divarshowads.R
import com.example.divarshowads.ui.base.SIDE_EFFECTS_KEY
import com.example.divarshowads.ui.component.LoadingLayout
import com.example.divarshowads.ui.component.NoDataScreen
import com.example.divarshowads.ui.component.ShowSnackBar
import com.example.divarshowads.ui.home.HomeContract
import com.example.divarshowads.ui.home.composable.adList.bottomSheet.CityBottomSheetContent
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._28
import com.example.divarshowads.ui.theme.white
import com.example.divarshowads.utils.ConnectionState
import com.example.divarshowads.utils.NetworkUtils.connectivityState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@SuppressLint("SuspiciousIndentation")
@ExperimentalCoroutinesApi
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AdListScreen(
    state: HomeContract.State,
    effectFlow: Flow<HomeContract.Effect>?,
    onEventSent: (event: HomeContract.Event) -> Unit,
    onNavigationRequested: (navigationEffect: HomeContract.Effect.Navigation) -> Unit,
) {

    val sheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val coroutine = rememberCoroutineScope()
    var citySearchQuery: String by remember { mutableStateOf("") }


    val connection by connectivityState()
    val isConnected = connection === ConnectionState.Available

    LaunchedEffect(SIDE_EFFECTS_KEY) {

        effectFlow?.onEach { effect ->
            when (effect) {
                HomeContract.Effect.Navigation.Back -> {
                    onNavigationRequested(HomeContract.Effect.Navigation.Back)
                }

                is HomeContract.Effect.Navigation.ToDetailScreen -> {
                    onNavigationRequested(
                        HomeContract.Effect.Navigation.ToDetailScreen(
                            postToken = effect.postToken,
                            adTitle = effect.adTitle
                        )
                    )
                }

                is HomeContract.Effect.Visibility.BottomSheetVisibility -> {
                    bottomSheetVisibility(coroutine, sheetState)
                }

                HomeContract.Effect.ClearUserInput -> {
                    citySearchQuery = ""
                }
            }
        }?.collect() {}
    }



    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetShape = RoundedCornerShape(topStart = _28, topEnd = _28),
        sheetContent = {
            CityBottomSheetContent(
                cityList = state.cityList,
                cityListResultSearch = state.cityListResultSearch,
                onCitySearchLayoutTextChange = {
                    citySearchQuery = it
                    onEventSent(HomeContract.Event.InputTextChangeIntCitySearchLayout(citySearchQuery))
                } ,
                onCityItemClick = { onEventSent(HomeContract.Event.OnCityListItemClick(it)) },
                onDeleteInputClick = { onEventSent(HomeContract.Event.OnClearUserInputClick) },

            )
        }
    ) {
        CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(white)
            ) {

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = Dimens._18, vertical = Dimens._24)
                ) {

                    SelectCityLayout(
                        value = state.currentCitySelected?.name,
                        onClick = { onEventSent(HomeContract.Event.OnSelectCityLayoutClick) }

                    )

                    Spacer(modifier = Modifier.height(Dimens._22))

                    if (state.advertisementList?.collectAsLazyPagingItems() == null)
                        NoDataScreen()
                    else
                        state.advertisementList.collectAsLazyPagingItems().let { adsList ->
                            AdvertisementList(
                                list = adsList,
                                onClick = { onEventSent(HomeContract.Event.OnAdvertisementClick(it)) }
                            )
                        }

                }
                when {

                    state.isLoading -> {
                        LoadingLayout()
                    }

                    state.isError -> state.throwable?.let {}


                    else -> {}
                }

                if (isConnected && state.needToRetryApiCall == true) {
                    onEventSent(HomeContract.Event.RetryApiCall)
                } else if (!isConnected && state.needToRetryApiCall == true || !isConnected ) {
                    ShowSnackBar(stringResource(R.string.not_connected_to_internet))
                    onEventSent(HomeContract.Event.InternetUnAvailable)
                }
            }

        }
    }
}


@OptIn(ExperimentalMaterialApi::class)
fun bottomSheetVisibility(coroutineScope: CoroutineScope, sheetState: ModalBottomSheetState) {
    coroutineScope.launch {
        if (sheetState.currentValue == ModalBottomSheetValue.Hidden) {
            sheetState.show()
        } else {
            sheetState.hide()
        }
    }
}


@OptIn(ExperimentalCoroutinesApi::class)
@Preview(showBackground = true)
@Composable
fun AdListScreenPreview() {
    AdListScreen(
        state = HomeContract.State(
            isLoading = true,
            isError = false,
            throwable = null,
            cityList = null,
            currentCitySelected = null,
            advertisementList = null,
            selectedAdvertisement = null,
            advertisementDetail = null,
        ),
        effectFlow = null,
        onEventSent = {},
        onNavigationRequested = {}
    )
}

