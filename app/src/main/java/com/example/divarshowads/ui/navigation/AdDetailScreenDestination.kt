package com.example.divarshowads.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.example.divarshowads.ui.home.HomeViewModel
import com.example.divarshowads.ui.home.HomeContract
import com.example.divarshowads.ui.home.composable.adDetail.AdDetailScreen

@Composable
fun AdDetailScreenDestination(
    navController: NavController,
    viewModel: HomeViewModel,
    postToken: String,
    adTitle: String
) {

    AdDetailScreen(
        adTitle = adTitle,
        postToken = postToken,
        state = viewModel.viewState.value,
        effectFlow = viewModel.effect,
        onEventSent = { event -> viewModel.setEvent(event) },

        onNavigationRequested = { navigationEffect ->
            if (navigationEffect is HomeContract.Effect.Navigation.Back) {
                navController.popBackStack()
            }
        }
    )

}
