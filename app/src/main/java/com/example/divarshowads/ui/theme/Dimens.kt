package com.example.divarshowads.ui.theme

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import com.example.divarshowads.R

object Dimens {


    val _1: Dp
        @Composable get() = dimensionResource(id = R.dimen._1)

    val _2: Dp
        @Composable get() = dimensionResource(id = R.dimen._2)

    val _4: Dp
        @Composable get() = dimensionResource(id = R.dimen._4)

    val _5: Dp
        @Composable get() = dimensionResource(id = R.dimen._5)

    val _6: Dp
        @Composable get() = dimensionResource(id = R.dimen._6)

    val _8: Dp
        @Composable get() = dimensionResource(id = R.dimen._8)

    val _10: Dp
        @Composable get() = dimensionResource(id = R.dimen._10)

    val _12: Dp
        @Composable get() = dimensionResource(id = R.dimen._12)

    val _13: Dp
        @Composable get() = dimensionResource(id = R.dimen._13)

    val _15: Dp
        @Composable get() = dimensionResource(id = R.dimen._15)

    val _16: Dp
        @Composable get() = dimensionResource(id = R.dimen._16)

    val _18: Dp
        @Composable get() = dimensionResource(id = R.dimen._18)

    val _20: Dp
        @Composable get() = dimensionResource(id = R.dimen._20)

    val _22: Dp
        @Composable get() = dimensionResource(id = R.dimen._22)

    val _24: Dp
        @Composable get() = dimensionResource(id = R.dimen._24)

    val _27: Dp
        @Composable get() = dimensionResource(id = R.dimen._27)

    val _28:Dp
        @Composable get() = dimensionResource(id = R.dimen._28)

    val _32:Dp
        @Composable get() = dimensionResource(id = R.dimen._32)

    val _60: Dp
        @Composable get() = dimensionResource(id = R.dimen._60)

    val _118: Dp
        @Composable get() = dimensionResource(id = R.dimen._118)

    val _400: Dp
        @Composable get() = dimensionResource(id = R.dimen._400)
}
