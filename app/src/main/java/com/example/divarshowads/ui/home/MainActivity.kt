package com.example.divarshowads.ui.home

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.example.divarshowads.ui.navigation.HomeNavigation
import com.example.divarshowads.ui.theme.DivarShowAdsTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            DivarShowAdsTheme(darkTheme = false) {
                HomeNavigation()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MainActivityPreview() {
    DivarShowAdsTheme {
        HomeNavigation()
    }
}