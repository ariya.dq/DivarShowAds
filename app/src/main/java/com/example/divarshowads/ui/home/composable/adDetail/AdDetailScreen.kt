package com.example.divarshowads.ui.home.composable.adDetail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.ui.base.SIDE_EFFECTS_KEY
import com.example.divarshowads.ui.component.CustomToolbar
import com.example.divarshowads.ui.component.LoadingLayout
import com.example.divarshowads.ui.component.ShowSnackBar
import com.example.divarshowads.ui.component.buttons.CustomFilledButtons
import com.example.divarshowads.ui.home.HomeContract
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Dimens._20
import com.example.divarshowads.ui.theme.Dimens._27
import com.example.divarshowads.ui.theme.primary
import com.example.divarshowads.ui.theme.white
import com.example.divarshowads.utils.ConnectionState
import com.example.divarshowads.utils.Constant.TOOLBAR_MAX_CHARACTER
import com.example.divarshowads.utils.NetworkUtils.connectivityState
import com.example.divarshowads.utils.limitCharacters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AdDetailScreen(
    postToken: String,
    adTitle: String,
    state: HomeContract.State,
    effectFlow: Flow<HomeContract.Effect>?,
    onEventSent: (event: HomeContract.Event) -> Unit,
    onNavigationRequested: (navigationEffect: HomeContract.Effect.Navigation) -> Unit,
) {

    val sheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val coroutine = rememberCoroutineScope()
    val connection by connectivityState()
    val isConnected = connection === ConnectionState.Available

    LaunchedEffect(SIDE_EFFECTS_KEY) {

        onEventSent(HomeContract.Event.GetAdvertisementDetailApiCall(postToken))

        effectFlow?.onEach { effect ->
            when (effect) {
                HomeContract.Effect.Navigation.Back -> {
                    onNavigationRequested(HomeContract.Effect.Navigation.Back)
                }

                is HomeContract.Effect.Visibility.BottomSheetVisibility -> {
                    bottomSheetVisibility(coroutine, sheetState)
                }

                else -> {}
            }
        }?.collect() {}
    }

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(white)
        ) {

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = Dimens._18, vertical = Dimens._24)
            ) {

                CustomToolbar(
                    title = adTitle.limitCharacters(TOOLBAR_MAX_CHARACTER),
                    onClick = { onEventSent(HomeContract.Event.BackButtonClicked) }
                )

                Spacer(modifier = Modifier.height(Dimens._24))

                state.advertisementDetail?.widgets?.let { GenerateListViewItems(it) }
            }

            if (state.advertisementDetail?.enableContact == true) {
                Box(
                    modifier = Modifier
                        .align(alignment = Alignment.BottomCenter)
                        .padding(vertical = _27, horizontal = _20)
                ) {
                    state.advertisementDetail.contactButtonText?.let {
                        CustomFilledButtons(
                            btnText = it,
                            onClick = {},
                            modifier = Modifier.fillMaxWidth(),
                            textColor = white,
                            backgroundBtnColor = primary
                        )
                    }

                }
            }

            when {
                state.isLoading -> {
                    LoadingLayout()
                }

                state.isError -> state.throwable?.let { }

                else -> {}
            }

            if (isConnected && state.needToRetryApiCall == true) {
                onEventSent(HomeContract.Event.RetryApiCall)
            } else if (!isConnected && state.needToRetryApiCall == true || !isConnected) {
                ShowSnackBar(stringResource(R.string.not_connected_to_internet))
                onEventSent(HomeContract.Event.InternetUnAvailable)
            }
        }


    }
}


@OptIn(ExperimentalMaterialApi::class)
fun bottomSheetVisibility(coroutineScope: CoroutineScope, sheetState: ModalBottomSheetState) {
    coroutineScope.launch {
        if (sheetState.currentValue == ModalBottomSheetValue.Hidden) {
            sheetState.show()
        } else {
            sheetState.hide()
        }
    }
}


@Preview(showBackground = true)
@Composable
fun AdDetailScreenPreview() {
    AdDetailScreen(
        adTitle = stringResource(id = R.string.ad_detail_toolbar_title),
        postToken = stringResource(R.string.sample_post_token),
        state = HomeContract.State(
            isLoading = true,
            isError = false,
            throwable = null,
            cityList = null,
            currentCitySelected = null,
            advertisementList = null,
            selectedAdvertisement = null,
            advertisementDetail = null,
        ),
        effectFlow = null,
        onEventSent = {},
        onNavigationRequested = {}
    )
}

