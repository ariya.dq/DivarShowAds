package com.example.divarshowads.ui.component

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.inversePrimary



@Composable
fun DrawEllipse(size: Dp = Dimens._8, color: Color = inversePrimary) {
    Canvas(
        modifier = Modifier
            .size(size),
        onDraw = {
            drawCircle(color)
        }
    )
}

@Preview(showBackground = true, widthDp = 150, heightDp = 50)
@Composable
fun DrawEllipsePreview() {
    DrawEllipse()
}

