package com.example.divarshowads.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens._16
import com.example.divarshowads.ui.theme.Dimens._20
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.gray2
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.white

@Composable
fun NoDataScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(white)
            .padding(_20),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.img_no_data),
            contentDescription = stringResource(R.string.no_data),
        )

        Spacer(modifier = Modifier.height(_16))

        Text(
            text = stringResource(R.string.no_data),
            color = gray2,
            fontSize = font_14,
            fontWeight = FontWeight.Normal,
            fontFamily = iranyekanFamily,
            lineHeight = font_20,
            style = MaterialTheme.typography.labelMedium,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewNoDataScreen() {
    NoDataScreen()
}