package com.example.divarshowads.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.example.divarshowads.ui.home.HomeViewModel
import com.example.divarshowads.ui.home.composable.adList.AdListScreen
import com.example.divarshowads.ui.home.HomeContract
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun AdListScreenDestination(navController: NavController, viewModel: HomeViewModel) {

    AdListScreen(
        state = viewModel.viewState.value,
        effectFlow = viewModel.effect,
        onEventSent = { event -> viewModel.setEvent(event) },

        onNavigationRequested = { navigationEffect ->
            if (navigationEffect is HomeContract.Effect.Navigation.Back) {
                navController.popBackStack()
            }

            if (navigationEffect is HomeContract.Effect.Navigation.ToDetailScreen) {
                navController.navigate(route = "AdDetailScreen/${navigationEffect.postToken}/${navigationEffect.adTitle}")
            }
        }
    )

}
