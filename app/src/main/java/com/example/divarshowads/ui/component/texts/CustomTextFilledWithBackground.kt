package com.example.divarshowads.ui.component.texts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.TextUnit
import com.example.divarshowads.R
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.font_14
import com.example.divarshowads.ui.theme.font_20
import com.example.divarshowads.ui.theme.green2
import com.example.divarshowads.ui.theme.green2_10
import com.example.divarshowads.ui.theme.iranyekanFamily
import com.example.divarshowads.ui.theme.primary
import com.example.divarshowads.ui.theme.primary_10
import com.example.divarshowads.ui.theme.red3
import com.example.divarshowads.ui.theme.red_2
import com.example.divarshowads.ui.theme.replyLightOnPrimary


@Composable
fun FilledText(
    title: String,
    titleColor: Color,
    backgroundColor: Color,
    verticalPadding: Dp = Dimens._4,
    horizontalPadding: Dp = Dimens._6,
    modifier: Modifier = Modifier,
    fontSize: TextUnit = font_14,
    lineHeight: TextUnit = font_20,
    borderRadius: Dp = Dimens._20,
    textAlign: TextAlign = TextAlign.Start,
    fontWeight: FontWeight? = FontWeight.Normal,
    fontFamily: FontFamily? = iranyekanFamily,
    style: TextStyle? = MaterialTheme.typography.bodyMedium,
) {

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Text(
            text = title,
            fontSize = fontSize,
            fontWeight = fontWeight,
            fontFamily = fontFamily,
            style = style!!,
            lineHeight = lineHeight,
            color = titleColor,
            textAlign = textAlign,
            modifier = modifier
                .background(color = backgroundColor, shape = RoundedCornerShape(borderRadius))
                .padding(vertical = verticalPadding, horizontal = horizontalPadding)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun FilledTextWithDarkGreenBackgroundPreview() {
    FilledText(
        title = stringResource(id = R.string.app_name),
        titleColor = replyLightOnPrimary,
        backgroundColor = green2,
    )
}

@Preview(showBackground = true)
@Composable
fun FilledTextLightGreenBackgroundPreview() {
    FilledText(
        title = stringResource(id = R.string.app_name),
        titleColor = green2,
        backgroundColor = green2_10,
    )
}

@Preview(showBackground = true)
@Composable
fun FilledTextWithDarkPrimaryBackgroundPreview() {
    FilledText(
        title = stringResource(id = R.string.app_name),
        titleColor = replyLightOnPrimary,
        backgroundColor = primary,
    )
}

@Preview(showBackground = true)
@Composable
fun FilledTextWithLightPrimaryBackgroundPreview() {
    FilledText(
        title = stringResource(id = R.string.app_name),
        titleColor = primary,
        backgroundColor = primary_10,
    )
}

@Preview(showBackground = true)
@Composable
fun FilledTextWithRedBackgroundPreview() {
    FilledText(
        title = stringResource(id = R.string.app_name),
        titleColor = red3,
        backgroundColor = red_2,
    )
}