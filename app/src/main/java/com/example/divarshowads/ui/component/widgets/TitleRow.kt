package com.example.divarshowads.ui.component.widgets


import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.ui.component.texts.CustomTextField


@Composable
fun TitleRow(
    title: String
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
        CustomTextField(title = "$title: ")
    }
}

@Preview(showBackground = true)
@Composable
fun TitleRowPreview() {
    TitleRow(title = stringResource(R.string.title_row_sample_title))
}
