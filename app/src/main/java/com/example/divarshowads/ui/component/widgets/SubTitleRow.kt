package com.example.divarshowads.ui.component.widgets


import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.ui.component.texts.CustomTextField
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.onSurfaceVariant


@Composable
fun SubTitleRow(
    title: String
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
        CustomTextField(
            title = title,
            fontSize = font_12,
            fontWeight = FontWeight.Normal,
            textStyle = MaterialTheme.typography.labelSmall,
            lineHeight = font_16,
            textColor = onSurfaceVariant,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SubTitleRowPreview() {
    SubTitleRow(title = stringResource(R.string.sub_title_row_sample_title))
}
