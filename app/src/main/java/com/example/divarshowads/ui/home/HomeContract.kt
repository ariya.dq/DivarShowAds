package com.example.divarshowads.ui.home


import androidx.paging.PagingData
import com.example.divarshowads.data.remote.model.CityModelItem
import com.example.divarshowads.data.remote.model.AdvertisementDetailObjectModel
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.ui.base.ViewEvent
import com.example.divarshowads.ui.base.ViewSideEffect
import com.example.divarshowads.ui.base.ViewState
import kotlinx.coroutines.flow.Flow

class HomeContract {

    sealed class Event : ViewEvent {
        object RetryApiCall : Event()
        object BackButtonClicked : Event()
        object InternetUnAvailable : Event()
        object OnClearUserInputClick : Event()
        object OnSelectCityLayoutClick : Event()
        data class GetAdvertisementDetailApiCall(val postToken: String) : Event()
        data class InputTextChangeIntCitySearchLayout(val userInput: String?) : Event()
        data class OnCityListItemClick(val selectedCityModelItem: CityModelItem) : Event()
        data class OnAdvertisementClick(val selectedAdvertisement: WidgetModelItem) : Event()

    }

    data class State(
        val isError: Boolean,
        val isLoading: Boolean,
        val throwable: Throwable?,
        val cityList: List<CityModelItem>? = null,
        val cityListResultSearch: List<CityModelItem>? = null,
        val currentCitySelected: CityModelItem?,
        val selectedAdvertisement: WidgetModelItem?,
        val advertisementDetail: AdvertisementDetailObjectModel?,
        val advertisementList: Flow<PagingData<WidgetModelItem>>?,
        val needToRetryApiCall : Boolean ? = true,
    ) : ViewState

    sealed class Effect : ViewSideEffect {

        object ClearUserInput : Effect()

        sealed class Visibility : Effect() {
            object BottomSheetVisibility : Visibility()
        }

        sealed class Navigation : Effect() {
            object Back : Navigation()
            data class ToDetailScreen(val postToken: String?, val adTitle : String?) : Navigation()

        }
    }

}