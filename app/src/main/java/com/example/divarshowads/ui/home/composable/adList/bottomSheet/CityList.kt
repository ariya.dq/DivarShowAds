package com.example.divarshowads.ui.home.composable.adList.bottomSheet

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.data.remote.model.CentroidModelItem
import com.example.divarshowads.data.remote.model.CityModelItem

@Composable
fun CityList(
    list: List<CityModelItem>,
    resultSearchList: List<CityModelItem>? = null,
    onClick: (CityModelItem) -> Unit,
) {
    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
        LazyColumn {
            items(resultSearchList ?: list) { item ->
                CityListItem(
                    modelItem = item,
                    onClick = { onClick.invoke(it) }
                )
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun CityListPreview() {
    CityList(
        list = listOf(
            CityModelItem(
                name = "تهران",
                id = 1,
                slug = "tehran",
                radius = 25000,
                centroidModelItem = CentroidModelItem(
                    latitude = 35.717358,
                    longitude = 51.375076
                )
            ),
            CityModelItem(
                name = "شیراز",
                id = 1,
                slug = "shiraz",
                radius = 25000,
                centroidModelItem = CentroidModelItem(
                    latitude = 35.717358,
                    longitude = 51.375076
                )
            )

        ),
        onClick = {}
    )
}