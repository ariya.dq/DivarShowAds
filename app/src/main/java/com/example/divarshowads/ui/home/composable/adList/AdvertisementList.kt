package com.example.divarshowads.ui.home.composable.adList

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import com.example.divarshowads.data.remote.model.WidgetModelItem
import com.example.divarshowads.ui.component.texts.CustomTextField
import com.example.divarshowads.ui.component.widgets.PostRow
import com.example.divarshowads.ui.component.widgets.TitleRow
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.onSurfaceVariant
import com.example.divarshowads.utils.Utils.Loading
import com.example.divarshowads.utils.ViewType

@Composable
fun AdvertisementList(
    list: LazyPagingItems<WidgetModelItem>,
    onClick: (WidgetModelItem) -> Unit
) {

    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        items(list.itemSnapshotList) { item ->
            when (item?.widgetType) {

                ViewType.TITLE_ROW.name -> {
                    item.text?.let { TitleRow(title = it) }
                }

                ViewType.SUBTITLE_ROW.name -> {
                    item.text?.let {
                        CustomTextField(
                            title = it,
                            fontSize = font_12,
                            fontWeight = FontWeight.Normal,
                            textStyle = MaterialTheme.typography.labelSmall,
                            lineHeight = font_16,
                            textColor = onSurfaceVariant,
                        )
                    }
                }

                ViewType.POST_ROW.name -> {
                    PostRow(itemModel = item, onClick = { onClick.invoke(it) })
                }
            }
        }
        when (val states = list.loadState.append) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {}
        }
        when (val states = list.loadState.prepend) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {}
        }
        when (val states = list.loadState.refresh) {
            is LoadState.NotLoading -> Unit
            is LoadState.Loading -> {
                Loading()
            }
            is LoadState.Error -> {}
        }
    }

}
