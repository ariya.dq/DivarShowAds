package com.example.divarshowads.ui.component


import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.divarshowads.utils.Utils.Loading


@Composable
fun LoadingLayout() {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Loading()
    }
}

@Preview(showBackground = true)
@Composable
fun LoadingLayoutPreview() {
    LoadingLayout()
}
