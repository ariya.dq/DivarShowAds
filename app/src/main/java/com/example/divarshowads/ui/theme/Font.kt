package com.example.divarshowads.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import com.example.divarshowads.R

val iranyekanFamily = FontFamily(
    Font(R.font.iranyekan_mobile_light, FontWeight.Light),
    Font(R.font.iranyekan_mobile_regular, FontWeight.Normal),
    Font(R.font.iranyekan_mobile_bold, FontWeight.Bold)
)