package com.example.divarshowads.ui.component.widgets


import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.AdvertisementDetailModelItem
import com.example.divarshowads.ui.component.DrawEllipse
import com.example.divarshowads.ui.component.texts.CustomTextField
import com.example.divarshowads.ui.theme.Dimens._5
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.onSurfaceVariant

@Composable
fun InfoRow(
    itemModel: AdvertisementDetailModelItem,
) {

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {

            DrawEllipse()

            Spacer(modifier = Modifier.width(_5))

            itemModel.title?.let { CustomTextField("$it: ") }

            Spacer(modifier = Modifier.width(_5))

            itemModel.value?.let {
                CustomTextField(
                    title = it,
                    fontSize = font_12,
                    fontWeight = FontWeight.Normal,
                    textStyle = MaterialTheme.typography.labelSmall,
                    lineHeight = font_16,
                    textColor = onSurfaceVariant,
                )
            }
        }

    }

}

@Preview(showBackground = true)
@Composable
fun InfoRowPreview() {
    InfoRow(
        itemModel = AdvertisementDetailModelItem(
            title = stringResource(R.string.info_row_sample_title),
            value = stringResource(R.string.info_row_sample_value),
        ),
    )
}
