package com.example.divarshowads.ui.component.widgets


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.Item
import com.example.divarshowads.ui.theme.Dimens
import com.example.divarshowads.ui.theme.Shapes
import com.example.divarshowads.ui.theme.onPrimary
import com.example.divarshowads.ui.theme.white_20
import com.example.divarshowads.ui.theme.white_25
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.delay

@OptIn(ExperimentalPagerApi::class, ExperimentalGlideComposeApi::class)
@Composable
fun ImageSliderRow(pages: List<Item>) {
    val configuration = LocalConfiguration.current

    val screenWidth = configuration.screenWidthDp.dp
    val screenHeight = screenWidth / 3
    val pagerState = rememberPagerState()

    HorizontalPager(
        count = pages.size,
        state = pagerState,
    ) { page ->

        Box(contentAlignment = Alignment.BottomStart) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(screenHeight)
                    .clip(shape = Shapes.medium),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                GlideImage(
                    model = pages[page].imageUrl,
                    contentDescription = "",
                    alignment = Alignment.CenterStart,
                    contentScale = ContentScale.Crop,
                    )
            }
            DotsIndicator(
                totalDots = pages.size,
                selectedIndex = pagerState.currentPage,
                selectedColor = onPrimary,
                unSelectedColor = white_20
            )
        }
    }

    LaunchedEffect(pagerState.currentPage) {
        delay(3000)
        var newPosition = pagerState.currentPage + 1
        if (newPosition > pages.size - 1) newPosition = 0
        pagerState.scrollToPage(newPosition)
    }

}

@Composable
fun DotsIndicator(
    totalDots: Int,
    selectedIndex: Int,
    selectedColor: Color,
    unSelectedColor: Color,
) {

    Box(
        modifier = Modifier
            .wrapContentSize()
            .padding(start = Dimens._16, bottom = Dimens._8)
            .clip(Shapes.small)
            .background(white_25),

        ) {

        LazyRow(
            modifier = Modifier
                .padding(Dimens._2)
        ) {

            items(totalDots) { index ->
                if (index == selectedIndex) {
                    Box(
                        modifier = Modifier
                            .size(Dimens._6)
                            .clip(CircleShape)
                            .background(selectedColor)
                    )
                } else {
                    Box(
                        modifier = Modifier
                            .size(Dimens._6)
                            .clip(CircleShape)
                            .background(unSelectedColor)
                    )
                }

                if (index != totalDots - 1) {
                    Spacer(modifier = Modifier.padding(horizontal = 1.dp))
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ImageSliderRowPreview() {
    ImageSliderRow(
        pages = listOf(
            Item(
                imageUrl = stringResource(R.string.image_slider_sample_image_url_1)
            ),
            Item(
                imageUrl = stringResource(R.string.image_slider_sample_image_url_2)
            ),
            Item(
                imageUrl = stringResource(R.string.image_slider_sample_image_url_3)
            ),
        )
    )
}