package com.example.divarshowads.ui.component.widgets


import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.divarshowads.R
import com.example.divarshowads.data.remote.model.AdvertisementDetailModelItem
import com.example.divarshowads.ui.component.texts.CustomTextField
import com.example.divarshowads.ui.theme.Dimens._12
import com.example.divarshowads.ui.theme.font_12
import com.example.divarshowads.ui.theme.font_16
import com.example.divarshowads.ui.theme.onSurface
import com.example.divarshowads.ui.theme.surface1

@Composable
fun DescriptionRow(
    itemModel: AdvertisementDetailModelItem,
) {

    CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {

        Card(
            shape = RoundedCornerShape(_12),
            colors = CardDefaults.cardColors(containerColor = surface1),
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(RoundedCornerShape(_12))
        ) {
            itemModel.subtitle?.let {
                CustomTextField(
                    title = it,
                    fontSize = font_12,
                    fontWeight = FontWeight.Normal,
                    textStyle = MaterialTheme.typography.labelSmall,
                    lineHeight = font_16,
                    textColor = onSurface,
                )
            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun DescriptionRowPreview() {
    DescriptionRow(
        itemModel = AdvertisementDetailModelItem(
            title = stringResource(R.string.desc_row_sample_title),
            subtitle = stringResource(R.string.desc_row_sample_sub_title),
        ),
    )
}
