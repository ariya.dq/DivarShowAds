package com.example.divarshowads

import android.app.Application
import com.example.divarshowads.data.local.UserPreferencesRepository
import com.example.divarshowads.utils.Constant.TOKEN
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltAndroidApp
open class App : Application(){

    @Inject
    lateinit var userPreferencesRepository: UserPreferencesRepository

    override fun onCreate() {
        super.onCreate()
        CoroutineScope(Dispatchers.Default).launch {
            userPreferencesRepository.storeToken(token = TOKEN)
        }
    }
}

