package com.example.divarshowads.utils

enum class ViewType{
    TITLE_ROW,
    SUBTITLE_ROW,
    POST_ROW,
    IMAGE_SLIDER_ROW,
    HEADER_ROW,
    INFO_ROW,
    DESCRIPTION_ROW,
}

