package com.example.divarshowads.utils


fun String.limitCharacters(maxLength: Int): String {
    return if (this.length > maxLength)
        this.substring(0, maxLength) + "..."
    else
        this

}

fun String.removeSlash(): String {
    return this.replace('/', '،').replace('\\', '،')
}
