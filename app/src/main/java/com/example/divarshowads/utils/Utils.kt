package com.example.divarshowads.utils


import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.ui.Modifier
import com.example.divarshowads.ui.theme.Dimens

object Utils {

    fun LazyListScope.Loading() {
        item {
            CircularProgressIndicator(modifier = Modifier.padding(Dimens._16))
        }
    }

}