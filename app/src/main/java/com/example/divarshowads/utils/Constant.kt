package com.example.divarshowads.utils

object Constant {

    const val BASE_URL = "https://android-interview.divar.dev/api/v1/"
    const val USER_PREFERENCES = "divar"
    const val TOKEN = "YXBpa2V5OjY5Y1dxVW8wNGhpNFdMdUdBT2IzMmRXZXQjsllsVzBtSkNiwU9yLUxEamNDUXFMSzJnR29mS3plZg=="

    const val DEFAULT_PAGE_INDEX = 1
    const val DEFAULT_PAGE_SIZE = 24

    const val DATABASE_NAME = "app.db"
    const val TOOLBAR_MAX_CHARACTER = 30

}