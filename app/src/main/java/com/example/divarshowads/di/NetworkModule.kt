package com.example.divarshowads.di

import com.example.divarshowads.data.local.UserPreferencesRepository
import com.example.divarshowads.data.remote.interceptor.AuthorizationInterceptor
import com.example.divarshowads.data.remote.service.ApiService
import com.example.divarshowads.utils.Constant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideAuthorizationInterceptor(userPreferencesRepository: UserPreferencesRepository): AuthorizationInterceptor {

        return AuthorizationInterceptor(userPreferencesRepository)

    }


    @Provides
    @Singleton
    fun provideOkHttpProvider(
        authorizationInterceptor: AuthorizationInterceptor,
    ): OkHttpClient {

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        val clientBuilder = OkHttpClient.Builder()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(httpLoggingInterceptor)

        clientBuilder.interceptors().add(authorizationInterceptor)
        clientBuilder.connectTimeout(30L, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(10L, TimeUnit.SECONDS)
        clientBuilder.readTimeout(10L, TimeUnit.SECONDS)
        return clientBuilder.build()

    }


    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

    }


    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }


}