package com.example.divarshowads.di


import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.example.divarshowads.data.local.UserPreferencesRepository
import com.example.divarshowads.data.remote.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import com.example.divarshowads.data.repositoryImpl.AdsRepositoryImpl
import com.example.divarshowads.data.room.AppDatabase
import com.example.divarshowads.domain.repository.AdsRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideUserPreferencesRepository(preferences: DataStore<Preferences>): UserPreferencesRepository {
        return UserPreferencesRepository(preferences)
    }

    @Singleton
    @Provides
    fun providesAdvertisementListDao(
        database: AppDatabase
    ) = database.advertisementListDao()

    @Provides
    @Singleton
    fun provideAdsRepository(apiService: ApiService/*, database: AppDatabase*/): AdsRepository {
        return AdsRepositoryImpl(apiService/*, database*/)
    }


}